# stage 1
FROM node:alpine as node
WORKDIR /app
COPY . .
RUN npm install && npm run build

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/portafolios /usr/share/nginx/html
EXPOSE 80