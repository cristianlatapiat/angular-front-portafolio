import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Project } from "../models/project";
import { global } from "../models/global";


@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public url: string;
  constructor(
    private _http: HttpClient,
  ) {
    this.url = global.url;
  }

  saveProject(project: Project): Observable<any> {
    var headers = new HttpHeaders().set("Content-Type", "application/json");
    return this._http.post(this.url + "projects", project, { headers: headers });
  }

  // funcion que nos permite enviar un archivo a un backend
  uploadFile(urlCompleta: string, files: Array<File>, fileName: string) {

    return new Promise(function (resolve, reject) {

      // emula un fromulario en un objeto
      var formData = new FormData();
      // sinonimo de ajax,XMLHttpRequest objeto de peticiones asincronas de JS
      var xhr = new XMLHttpRequest();

      // recorren todos los archivos seleccionados
      for (let index = 0; index < files.length; index++) {

        // se los adjuntamos al formulario
        formData.append(fileName, files[index], files[index].name);
      }

      // hacemos la peticion ajax al backend
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          }
          else {
            reject(xhr.response);
          }
        }
      }

      xhr.open("POST", urlCompleta, true);
      xhr.send(formData);
    });
  }

  getProjects(): Observable<any> {
    return this._http.get(this.url + "projects");
  }

  getProjectById(id: string): Observable<any> {
    return this._http.get(this.url + "projects/" + id);
  }

  deleteProjectById(id: string): Observable<any> {
    let urlcom = this.url + "projects/" + id;
    return this._http.delete(urlcom);
  }

  updateProject(project: Project): Observable<any> {
    let urlcom = this.url + "projects/" + project._id;
    return this._http.put(urlcom, project);
  }
}
