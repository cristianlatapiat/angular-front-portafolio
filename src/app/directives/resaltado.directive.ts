import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(private el: ElementRef) {

  }

  ngOnInit() {
    var element = this.el.nativeElement;
    element.style.background = "red";
    element.style.width = "600px";
    element.innerText = element.innerText.toUpperCase();
  }

}
