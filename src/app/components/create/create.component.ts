import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { global } from "../../models/global";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { routing } from 'src/app/app.routing';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public isLoading: boolean;
  public title: string;
  public project: Project;
  public projecSaved: any;
  public status: string;
  public fileToUpload: Array<File>;
  public url: string;
  constructor(
    private _projectServices: ProjectService,
    private route: ActivatedRoute,
    private router: Router

  ) {
    this.title = "Crear Proyecto";
    this.project = new Project("", "", "", "", 2020, "", "");
    this.status = "";
    this.url = global.url + "projects/images/";

  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe((param) => {
      let id = param.id;
      if (id) {
        setTimeout(() => {
          this.getProjectById(id);
          this.title = "Editar Proyecto";
          this.isLoading = false;
        }, 1000);

      }
      else
      {
        this.isLoading = false;
      }
    });
  }

  getProjectById(id: string) {

    this._projectServices.getProjectById(id).subscribe(
      response => {
        this.project = response.project;
      },
      error => {
        console.log(<any>error);
      }

    );
  }

  onSubmit(form) {

    if (!this.project._id) {
      this._projectServices.saveProject(this.project).subscribe(
        response => {

          if (response.project) {
            this._projectServices.uploadFile(global.url + "upload-image/" + response.project._id, this.fileToUpload, "image").then((result: any) => {
              this.projecSaved = response.project;
              this.status = "OK";
              form.reset();
            });


          }
          else {
            this.status = "NOK";
          }

        },
        err => {
          console.log(<any>err);
        }

      );
    } else {
      this._projectServices.updateProject(this.project).subscribe(
        response => {
          this.projecSaved = response.project;
          if (this.fileToUpload) {
            this._projectServices.uploadFile(global.url + "upload-image/" + response.project._id, this.fileToUpload, "image").then((result: any) => {
              this.status = "OK";
            });
          }

          this.router.navigate(['/project/' + this.projecSaved._id]);

        },
        error => {

          console.log(error);
        }
      );
    }

  }



  fileChange(fileInput) {
    this.fileToUpload = <Array<File>>fileInput.target.files;
    console.log(this.fileToUpload);

  }

}
