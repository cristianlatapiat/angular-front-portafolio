import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { global } from 'src/app/models/global';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  providers: [ProjectService]
})
export class DetailComponent implements OnInit {

  public isLoading: boolean;
  public url: string;
  public project: Project;
  public confirm: boolean;
  constructor(
    public _projectServices: ProjectService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.project = new Project("", "", "", "", 0, "", "");
    this.url = global.url + "projects/images/";
    this.confirm = false;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe((param) => {
      let id = param.id;
      console.log(id);
      setTimeout(() => {
        this.getProjectById(id);
        this.isLoading = false;
      }, 1000);
    
    });
  }

  getProjectById(id: string) {
    this._projectServices.getProjectById(id).subscribe(
      response => {
        this.project = response.project;
      },
      err => {
        console.log(<any>err);
      }
    );
  }

  deleteProjectById(id: string) {
    this._projectServices.deleteProjectById(id).subscribe(
      response => {

        this.router.navigate(['/projects']);
      },
      error => {
        console.log(<any>error);
      }
    );

  }

  setConfirm(value: boolean) {
    this.confirm = value;

  }

}
