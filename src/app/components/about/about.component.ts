import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],

})
export class AboutComponent implements OnInit {

  public title: string;
  public subtitle: string;
  public email: string;

  modalRef: BsModalRef;

  constructor(

    private modalService: BsModalService
  ) {

    this.title = "Cristian Muñoz";
    this.subtitle = "Ingeniero de Software";
    this.email = "cristianlatapiat@gmail.com";

  }

  ngOnInit(): void {
  }

  openModal(template:TemplateRef<any>)
  {
    this.modalRef = this.modalService.show(template);

  }
}
