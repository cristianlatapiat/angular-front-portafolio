import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { global } from "../../models/global";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [ProjectService]
})
export class ProjectsComponent implements OnInit {
  public isLoading: boolean;
  public projects: Array<Project>;
  public url: string;
  constructor(
    private _projectServices: ProjectService
  ) {
    this.url = global.url + "projects/images/"
    this.projects = new Array<Project>();
  }

  ngOnInit(): void {
    this.isLoading = true;
    setTimeout(() => {
      this.getProjescts();
      this.isLoading = false;
    }, 1000);

  }

  getProjescts() {
    this._projectServices.getProjects().subscribe(
      response => {
        if (response.projecs) {
          this.projects = response.projecs;
        }


      }, err => {
        console.log(<any>err);
      });
  }
}
