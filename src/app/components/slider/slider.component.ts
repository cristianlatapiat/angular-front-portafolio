import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  // parametro usado para recibir datos desde el componente padre
  @Input() anchura: number;
  @Input() etiqueta: boolean;

  // para enviar informacion al componente padre
   @Output() setAutor = new EventEmitter()
  public autor: any;
  constructor() {

    this.autor = {
      nombre: "Cristian",
      email: "cristianlatapiat@gmail.com",
      edad: 38
    };
  }

  ngOnInit(): void {
    $('.bxslider').bxSlider({
      auto: true,
      autoControls: true,
      stopAutoOnClick: true,
      pager: this.etiqueta,
      slideWidth: this.anchura
    });
  }

  lanzar(event) {
    console.log(event);
    this.setAutor.emit(this.autor);
  }

}
