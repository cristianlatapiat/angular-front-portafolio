import { Component, OnInit, ViewChild ,AfterViewInit} from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit,AfterViewInit {

  public widthSlider: number;
  public caption: boolean;
  public autor: any;
  // decorador ViewChild
  //@ViewChild('textos') textos; disponible solo desdpues del evento  ngAfterViewInit
  @ViewChild('textos', {static: true}) textos; // funciona bien para el evento onInit
  constructor() {
    this.caption = false;
    this.autor = {
      nombre: "",
      email: "",
      edad: null
    };
  }
  ngAfterViewInit(){
    //console.log("ngAfterViewInit" ,this.textos);
  }

  ngOnInit(): void {
    //jquery
    console.log($("#texto"));
    // javascript tradicional
    console.log(document.getElementById("texto"));
    // objeto nativo de angular
    console.log(this.textos);
  }

  changeWidth(event) {
    this.widthSlider = event.target.value;
  }
  resetComponent() {
    this.widthSlider = null;
  }

  setAutor(event) {
    this.autor = event;
  }
}
